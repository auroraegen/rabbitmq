﻿using System;
using System.Text;
using RabbitMQ.Client;

namespace Producer
{
    public class Sender
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel()) {
                channel.QueueDeclare("BasicTest", false, false, false, null);

                string message = "Getting started with .NET Core RabbitMQ";

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish("", "BasicTest", null, body);

                Console.WriteLine("Sent message {0}..", message);

            }

            Console.WriteLine("Press any key to continue");
            // Console.ReadLine();
        }
    }
}
